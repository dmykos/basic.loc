<?php

namespace console\controllers;

use app\models\User;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    // yii rbac/init  // to create roles
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $auth->add($admin);

        $user = $auth->createRole('user');
        $user->description = 'User';
        $auth->add($user);

        $auth->addChild($admin, $user);
    }

    //yii rbac/assign-admin  // to assign admin role
    public function actionAssignAdmin()
    {
	    $user = new User();
	    $user->email = 'dmykos@gmail.com';
	    $user->username = 'admin';
	    $user->setPassword('111111');
	    $user->save();

        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');
        $auth->assign($adminRole, $user->getId());
    }

    //yii rbac/add-test-users  // to add test users and assign them to role user
    public function actionAddTestUsers()
    {
        $user1 = new User();
        $user1->email = 'vasya@gmail.com';
        $user1->username = 'vasya';
        $user1->setPassword('111111');
        $user1->save();

        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user1->getId());

        $user3 = new User();
        $user3->email = 'vlad@gmail.com';
        $user3->username = 'vlad';
        $user3->setPassword('111111');
        $user3->save();

        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user3->getId());
    }
}
